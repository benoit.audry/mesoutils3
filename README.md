
<!-- README.md is generated from README.Rmd. Please edit that file -->

# mesoutils3

Le but de ce package est d’avoir des infos sur un jeu de données

## Installation

You can install the development version of mesoutils3 like so:

``` r
# library(remotes)
remotes::install_gitlab("benoit.audry/mesoutils3")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(mesoutils3)
## basic example code
get_info_data(df = starwars_species)
#> $dimension
#> [1] 87  1
#> 
#> $names
#> [1] "species"
get_mean_data(df = iris)
#>   Sepal.Length Sepal.Width Petal.Length Petal.Width
#> 1     5.843333    3.057333        3.758    1.199333
```
